package dev.bilotta.util;

import java.sql.*;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {

        // jdbc connection string format -   jdbc:postgresql://host:port/database?currentSchema=(desiredSchema)

        String url = "jdbc:postgresql://trainingdatabase.cp2fk03jqwkt.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project0";
        final String password =  System.getenv("PASSWORD");
        final String username = System.getenv("USERNAME");

        if(connection==null || connection.isClosed()) {
            connection = DriverManager.getConnection(url, username, password);
        }

        return connection;
    }

}
