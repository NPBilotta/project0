package dev.bilotta.models;

import java.util.Objects;

public class Customer {

    private int id;
    private String email;
    private String password;
    private String name;

    //noArgs constructor, should not be needed
    public Customer() {
        super();
    }

    //This constructor SHOULD NOT be used by default, id column should auto-populate on the database
    public Customer(int id, String email, String password, String name) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
    }

    //Default constructor for use within the program
    public Customer(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }


    //getters and setters for the private class variables
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Overridden toString, equals, and hasCode methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id && Objects.equals(email, customer.email) && Objects.equals(password, customer.password) && Objects.equals(name, customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, name);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                '}';
    }


}
