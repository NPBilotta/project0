package dev.bilotta.models;

import java.util.Objects;

public class Transaction {

    private int id;
    private int accountNum;
    private String transType;
    private double adjustment;

    //noArgs constructor for transaction object
    public Transaction() {
        super();
    }

    //Regular constructor for use within the program
    public Transaction(int accountNum, String transType, double adjustment) {
        this.accountNum = accountNum;
        this.transType = transType;
        this.adjustment = adjustment;
    }

    //constructor for manually creating a new transID instead of auto-populating: NOT RECOMMENDED
    public Transaction(int id, int accountNum, String transType, double adjustment) {
        this.id = id;
        this.accountNum = accountNum;
        this.transType = transType;
        this.adjustment = adjustment;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public double getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(double adjustment) {
        this.adjustment = adjustment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id == that.id && accountNum == that.accountNum && Double.compare(that.adjustment, adjustment) == 0 && Objects.equals(transType, that.transType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNum, transType, adjustment);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", accountNum=" + accountNum +
                ", transType='" + transType + '\'' +
                ", adjustment=" + adjustment +
                '}';
    }


}
