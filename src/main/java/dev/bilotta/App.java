package dev.bilotta;

import dev.bilotta.util.ConnectionUtil;
import dev.bilotta.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class App {

    public static void main(String[] noArgs) throws SQLException {
        //TODO Call to the DB and populate objects (?)

        //call start up menu
        start();



    }

    private static void start() {
        System.out.println("Welcome to Bilotta Bank! Please enter the number of a corresponding sign-in option:" +
                "\n1. Sign in to an existing customer account" +
                "\n2. Register as a new customer");
    }




    //TODO add functionality to create and login(and logout) to accounts
    //Accounts will be stored in the database along with transactions
    //Checking and savings accounts as options, no functional difference required
    //BONUS: Multiple users can share one account (joint accounts)




    //TODO Withdrawal and Deposit functions with validation and no overdrafts
    //Transactions will be recorded into a remote database
    //BONUS: Functionality for transferring funds between accounts




    //TODO View account balance and transaction history
    //Pull from database to view transaction history and balance (proper formatting)
    //Adhere to data persistence (3NF tables, transaction blocks in sql queries, etc)



        /*
            Example query to use when pulling up account details from the M-M table:

             -- Getting all students for a class:

                SELECT s.student_id, last_name
                  FROM student_classes sc
            INNER JOIN students s ON s.student_id = sc.student_id
                 WHERE sc.class_id = X

             -- Getting all classes for a student:

                SELECT c.class_id, name
                  FROM student_classes sc
            INNER JOIN classes c ON c.class_id = sc.class_id
                 WHERE sc.student_id = Y

         */




}
